import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';
//import s3 module
import * as s3 from 'aws-cdk-lib/aws-s3';
export class SampleProjectStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    //Create an S3 bucket with versioning enabled and encryption
    const bucket = new cdk.aws_s3.Bucket(this, "hw3bucket", {
      versioned: true,
      encryption: cdk.aws_s3.BucketEncryption.KMS_MANAGED
    })
  }
}
