# Goal of this project

--Create S3 bucket using AWS CDK

--Use CodeWhisperer to generate CDK code

--Add bucket properties like versioning and encryption
![Example Image](s3bucket.png)
-It also has the properties of encrytion and versioning:

![Example Image](bucket.png)
![Example Image](bucket2.png)

## Envrionment set up

-Create a user in IAM with following access:
![Example Image](access.jpg)

-Generate access key and secret key to log in with that user

-Create a project for this mini-project

-run cdk init app --language typescript

-use code whiper to generate code to create a s3 bucket under the bin directory
![Example Image](codewhisper.png)

-run cdk bootstrap aws://ACCOUNT-NUMBER/REGION

-run cdk deploy
![Example Image](deploy.png)

# CodeWhisper Usage

-import S3 bucket in the lib file

-comment "create a S3 bucket with versioning and encryption", then press tab, the code will automatically show up.

-generated code is:

    import _ as cdk from 'aws-cdk-lib';
    import { Construct } from 'constructs';
    // import _ as sqs from 'aws-cdk-lib/aws-sqs';
    //import s3 module
    import \* as s3 from 'aws-cdk-lib/aws-s3';
    export class SampleProjectStack extends cdk.Stack {
    constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

        //Create an S3 bucket with versioning enabled and encryption
        const bucket = new cdk.aws_s3.Bucket(this, "hw3bucket", {
        versioned: true,
        encryption: cdk.aws_s3.BucketEncryption.KMS_MANAGED
        })

    }
    }
